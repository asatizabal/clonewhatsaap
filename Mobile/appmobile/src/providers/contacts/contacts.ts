import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { config } from "../config";

/*
  Generated class for the ContactosProvider provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular DI.
*/
@Injectable()
export class ContactsProvider {

  constructor(public http: Http) {
    console.log('Hello ContactosProvider Provider');
  }



  listarContactos(id){
    
   
      let url=config.SERVICES_LOCATION+"Contacto/ListarContactos?id="+id;
      console.log(url);
      return this.http.get(url).map(res=>res.json());
    
      
      }

      verificarContacto(id,id2){
        let url = config.SERVICES_LOCATION + "Contacto/verificarContactos?id=" + id + "&id2=" + id2;
         console.log(url);
        return this.http.get(url).map(res => res.json());
    
    
      }
      guardarContacto(id,id2){
        let url = config.SERVICES_LOCATION + "Contacto/GuardarContacto?id=" + id + "&id2=" + id2;
         console.log(url);
        return this.http.get(url).map(res => {});
    
    
      }
      

}
