import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';
import {config} from './config';
import { Api } from './api';

import 'rxjs/add/operator/map';

@Injectable()
export class User {
username: string;
loggedIn: boolean;
   constructor(private http: Http, public api: Api) {
  this.username = '';
  this.loggedIn = false;
   }


   putStatus(user){
    
        let url=config.SERVICES_LOCATION+"User/Status";
        let headers=new Headers({'Content-Type':'application/x-www-form-urlencoded'});
        let options=new RequestOptions({headers:headers});
        let data=this.jsonToUrlParams(user);
        //let data=JSON.stringify(user);
    
        return this.http.put(url,data,{headers:headers});
    
      }

   getById(id){
    let url = config.SERVICES_LOCATION+"User/ById/?id="+id;
    return this.http.get(url);
}

insertarCodigo(datos){
  
   
      let url=config.SERVICES_LOCATION+"Qr/CrearQr";
      let headers=new Headers({'Content-Type':'application/x-www-form-urlencoded'});
      let options=new RequestOptions({headers:headers});
      let data=this.jsonToUrlParams(datos);
      
      return this.http.post(url,data,{headers:headers}).map(res=>res.json());
    }
   getEstadoUsuario(){
    let url = config.SERVICES_LOCATION+"Estado_usuario/Index";
    return this.http.get(url);
}
   
   login(userInfo) {
      let url = config.SERVICES_LOCATION+"User/Login";
      let iJon = this.jsonToUrlParams(userInfo);
      console.log("este fue "+url);
      let headers=new Headers({'Content-Type':'application/x-www-form-urlencoded'});
      let options=new RequestOptions({headers:headers});
      return this.http.post(url, iJon,{headers:headers}).map(res => res.json()).map(res => {
        console.log(res[0]["token"]);
         if (res=="Valide los datos de usuario" || res=="nofound"){
            this.loggedIn = false;
           
         } 
         else if(res[0]["token"]) {
            localStorage.setItem('token', res[0]["token"]);
            localStorage.setItem('id', res[0]["id"]);
            this.username = userInfo.username;
             localStorage.setItem('username', this.username);
            this.loggedIn = true;
             }
             else {
              this.loggedIn = false;
         }
         
         return this.loggedIn;
      });
   }



   logout(): void {
      localStorage.removeItem('token');
      localStorage.removeItem('id');
      localStorage.removeItem('username');
      this.username = '';
      this.loggedIn = false;

    

   }



   isLoggedIn() {
      return this.loggedIn;
   }

   signup(accountInfo: any) {
    let seq = this.api.post('signup', accountInfo).share();

    seq.map(res => res.json())
      .subscribe();

    return seq;
  }

  jsonToUrlParams(json){
    
          var str=Object.keys(json)
                  .map(key => encodeURIComponent(key) + '=' + encodeURIComponent(json[key]))
                  .join("&");
    
          return ( str);
    
      }

}
