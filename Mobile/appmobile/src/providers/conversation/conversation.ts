import { Injectable } from '@angular/core';
import { Http, RequestOptions, Headers } from '@angular/http';
import 'rxjs/add/operator/map';
import { ChatMessage } from "../../models/conversation";

import 'rxjs/add/operator/toPromise';
import { config } from "../config";

/*
  Generated class for the ConversationProvider provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular DI.
*/
@Injectable()
export class ConversationProvider {

  constructor(public http: Http) {
    console.log('Hello ConversationProvider Provider');
  }

  getnewChats(datos){

    let url = config.SERVICES_LOCATION + "Mensaje/NewMessages";
    let headers = new Headers({ 'Content-Type': 'application/x-www-form-urlencoded' });
    let data = this.jsonToUrlParams(datos);
    let options = new RequestOptions({ headers: headers });
    return this.http.post(url, data, { headers: headers }).map(res=>res.json());

  }

  getMsgList(id, id2): Promise<ChatMessage[]> {
    let url = config.SERVICES_LOCATION + "Mensaje/Mensajes?id=" + id + "&id2=" + id2;
    return this.http.get(url).toPromise()
      .then(response => response.json() as ChatMessage[])
      .catch(err => Promise.reject(err || 'err'));

  }

  sendMsg(msg) {
    let url = config.SERVICES_LOCATION + "Mensaje/GuardarMensaje";
    let headers = new Headers({ 'Content-Type': 'application/x-www-form-urlencoded' });
    let data = this.jsonToUrlParams(msg);
    let options = new RequestOptions({ headers: headers });
    return this.http.post(url, data, { headers: headers }).map(res => {

    });

  }

  jsonToUrlParams(json) {

    var str = Object.keys(json)
      .map(key => encodeURIComponent(key) + '=' + encodeURIComponent(json[key]))
      .join("&");

    return (str);

  }

}
