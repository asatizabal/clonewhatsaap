import { Injectable } from '@angular/core';
import { Http, RequestOptions,Headers } from '@angular/http';
import 'rxjs/add/operator/map';
import { config } from "../config";

/*
  Generated class for the RestriccionProvider provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular DI.
*/
@Injectable()
export class RestriccionProvider {

  constructor(public http: Http) {
    console.log('Hello RestriccionProvider Provider');
  }

  postIdRestriccion(restriccion){

    let url=config.SERVICES_LOCATION+"Restriccion/Validar";
    let headers=new Headers({'Content-Type':'application/x-www-form-urlencoded'});
    let options=new RequestOptions({headers:headers});
    let data=this.jsonToUrlParams(restriccion);
      return this.http.post(url,data,{headers:headers});


  }

  putBlockUser(restriccion){

    let url=config.SERVICES_LOCATION+"Restriccion/Restriccion";
    let headers=new Headers({'Content-Type':'application/x-www-form-urlencoded'});
    let options=new RequestOptions({headers:headers});
    let data=this.jsonToUrlParams(restriccion);
    //let data=JSON.stringify(user);

    console.log(restriccion);

    return this.http.put(url,data,{headers:headers});

  }

  putUnBlockUser(restriccion){

    let url=config.SERVICES_LOCATION+"Restriccion/Desbloquear";
    let headers=new Headers({'Content-Type':'application/x-www-form-urlencoded'});
    let options=new RequestOptions({headers:headers});
    let data=this.jsonToUrlParams(restriccion);
    //let data=JSON.stringify(user);

    console.log(restriccion);

    return this.http.put(url,data,{headers:headers});

  }
    
  jsonToUrlParams(json){
        return Object.keys(json)
        .map( key => encodeURIComponent(key) + '=' + encodeURIComponent(json[key]) )
        .join("&");
}


}
