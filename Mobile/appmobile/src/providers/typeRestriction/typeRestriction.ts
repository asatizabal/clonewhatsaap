import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { config } from "../config";

/*
  Generated class for the TipoRestriccionProvider provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular DI.
*/
@Injectable()
export class TipoRestriccionProvider {

  constructor(public http: Http) {
    console.log('Hello TipoRestriccionProvider Provider');
  }

  getIdDescripcion($blockAction){
      let url = config.SERVICES_LOCATION+"Tipo_restriccion/Id?action="+$blockAction;
      console.log($blockAction);
      return this.http.get(url);
  }

}
