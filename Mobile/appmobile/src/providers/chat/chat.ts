import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/map';
import { config } from '../config';

/*
  Generated class for the ChatProvider provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular DI.
*/
@Injectable()
export class ChatProvider {

  constructor(public http: Http) {
  }

  crearChat(datos){

 
    let url=config.SERVICES_LOCATION+"Chat/CrearChat";
    let headers=new Headers({'Content-Type':'application/x-www-form-urlencoded'});
    let options=new RequestOptions({headers:headers});
    let data=this.jsonToUrlParams(datos);
    
    //let data=JSON.stringify(user);
    return this.http.post(url,data,{headers:headers}).map(res=>res.json());
  }

  ListarChats(id){
    
   
      let url=config.SERVICES_LOCATION+"Chat/ListarChats?id="+id;
      
      return this.http.get(url).map(res=>res.json());
    
      
      }
      eliminarChats(chats){
        
       
    
    let url=config.SERVICES_LOCATION+"Chat/EliminarChats";
    let headers=new Headers({'Content-Type':'application/x-www-form-urlencoded'});
    let options=new RequestOptions({headers:headers});
    let data=this.jsonToUrlParams(chats);
    
    //let data=JSON.stringify(user);
    return this.http.post(url,data,{headers:headers});
        
          
          }
    
      jsonToUrlParams(json){
        return Object.keys(json)
        .map( key => encodeURIComponent(key) + '=' + encodeURIComponent(json[key]) )
        .join("&");
      }

}
