import { Component, ViewChild } from '@angular/core';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { NavController, ViewController, Item, ModalController } from 'ionic-angular';
import { ContactsProvider } from "../../providers/contacts/contacts";
import { ItemDetailPage } from "../item-detail/item-detail";
import { ListMasterPage } from "../list-master/list-master";
import { TabsPage } from "../tabs/tabs";
import { ConversationPage } from "../conversation/conversation";
import { ChatProvider } from "../../providers/chat/chat";




@Component({
  selector: 'contacts',
  templateUrl: 'contacts.html'
})
export class ContactsPage {
  private currentItems: Item[];
  private items:any=[];
  
  @ViewChild('fileInput') fileInput;

  private id: any;
  searchQuery: string = '';



  constructor(public navCtrl: NavController, public viewCtrl: ViewController, formBuilder: FormBuilder,
    public misContactos: ContactsProvider,public modalCtrl: ModalController,public ChatProvider:ChatProvider) {
    this.id = localStorage.getItem("id");

    this.init();
    this.ListarContactoss();
    
  }

  init() {


  }

  ionViewDidLoad() {

  }



  ListarContactoss() {

    this.misContactos.listarContactos(this.id).subscribe(res => {
      console.log(res);

      this.currentItems = res;
      for(let i=0; i<this.currentItems.length; i++){
      
        this.items.push(this.currentItems[i]["nombre"]+" "+this.currentItems[i]["apellido"])
  
      }
    });


  }
  openItem(item: Item) {

    let datos={
      me:this.id,
      other:item["other"]
    }

this.ChatProvider.crearChat(datos).subscribe(res=>{
 
  if(res["res"]=="chat creado"){
   
    item["me"]=this.id;
    item["id"]=res["idChat"];
    console.log(item);
    let addModal = this.modalCtrl.create(ConversationPage,item);
    addModal.present();
  }

});

  

  }

  cancel(){

    this.navCtrl.setRoot(TabsPage);

  }

  

  getItems(ev: any) {
    this.items;
    // Reset items back to all of the items
    // set val to the value of the searchbar
   // console.log(this.currentItems[0]);
console.log( this.items);
    let val = ev.target.value;
    
        // if the value is an empty string don't filter the items
        if (val && val.trim() != '') {
          
          this.items = this.items.filter((item) => {
            console.log( "entro val" +val );
            
            console.log( "entro" +item.toLowerCase() );
            let vare = item.toLowerCase().indexOf(val.toLowerCase()) > -1;
            console.log( "entro  vare  " + vare );
            
            
            return (item.toLowerCase().indexOf(val.toLowerCase()) > -1);
          })
        }

    
        // if the value is an empty string don't filter the items
       
    
    // if the value is an empty string don't filter the items
   
  }

}
