import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { User } from '../../providers/user';
import { LoginPage } from '../login/login';
import { SignupPage } from '../signup/signup';
import { MainPage } from '../../pages/pages';
import { AlertController } from 'ionic-angular';
/**
 * The Welcome Page is a splash page that quickly describes the app,
 * and then directs the user to create an account or log in.
 * If you'd like to immediately put the user onto a login/signup page,
 * we recommend not using the Welcome page.
*/
@Component({
  selector: 'page-welcome',
  templateUrl: 'welcome.html'
})
export class WelcomePage {

  loggedIn: boolean;

  account: { username: string, password: string } = {
    username: 'username',
    password: 'test'
  };


  constructor(public navCtrl: NavController, public user: User,public alertCtrl:AlertController) { }


  login(){
    console.log(this.account);
        this.user.login(this.account)
                  .subscribe(
            rs => this.loggedIn = rs,
            err => console.log(err),
            () => {
              if (this.loggedIn){
                this.navCtrl.setRoot(MainPage)
               
              } else {
                let alert = this.alertCtrl.create({
                  title: 'Error',
                  subTitle: 'No se pudo Iniciar',
                  buttons: ['OK']
                });
                alert.present();
                console.log('Acceso denegado');
              }
            }
          )
      }




}
