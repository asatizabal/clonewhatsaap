import { Component } from '@angular/core';
import {NavController, NavParams } from 'ionic-angular';
import { TipoRestriccionProvider } from "../../providers/typeRestriction/typeRestriction";
import { Storage } from '@ionic/storage';
import { RestriccionProvider } from "../../providers/restriction/restriction";
import { Restriccion } from "../../models/restriccion";

/**
 * Generated class for the ContactProfilePage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */


@Component({
  selector: 'page-contact-profile',
  templateUrl: 'detail-contact-profile.html',
})
export class ContactProfilePage {

  private contact: any;
  private isABlockedUser: any = "false";
  private restriccion: Restriccion;
  private idRestriccion: any;
  private id : any;

  constructor(public navCtrl: NavController, public navParams: NavParams, private tipoRestriccion: TipoRestriccionProvider, public storage: Storage, public restriccionProvider: RestriccionProvider ) {
    
          this.contact = navParams.get('contact');

          console.log(this.contact);
    
           this.id = localStorage.getItem("id");
                this.restriccion = new Restriccion(null,this.id,this.contact.other,this.idRestriccion);
                this.restriccionProvider.postIdRestriccion(this.restriccion).subscribe(data=>{
                
                  console.log(data);
                let idArr = data.json();
                this.idRestriccion = idArr[0].id;
    
                if(this.idRestriccion==2){
                  this.isABlockedUser= "true"; 
    
                }else{
    
                  this.isABlockedUser= "false";
    
                }
                  
                console.log(data)
                });
            
      }

  ionViewDidLoad() {
    //console.log('ionViewDidLoad ContactProfilePage');
  }

  blockUser(){

    //console.log("bloquearUsuario");

    if(this.isABlockedUser=="false"){this.isABlockedUser= "true"; }else{ this.isABlockedUser= "false";}

    this.tipoRestriccion.getIdDescripcion(this.isABlockedUser).subscribe(res=>{
          //console.log(res);
          let idArr = res.json();
          this.idRestriccion = idArr[0].id;

          
          this.restriccion = new Restriccion(null,this.id,this.contact.other,this.idRestriccion);
            this.restriccionProvider.putUnBlockUser(this.restriccion).subscribe(data=>{console.log(data)});
          

      });

  
  }

}
