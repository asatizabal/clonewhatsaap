import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { User } from "../../providers/providers";
import { Storage } from '@ionic/storage';
import { StatePage } from "../state/state";

/**
 * Generated class for the DetailProfilePage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */


@Component({
  selector: 'page-detail-profile',
  templateUrl: 'detail-profile.html',
})
export class DetailProfilePage {

  id: any;
  username: any;
  name:any;
  lastname: any;
  user: any;
  cellphone:any;
  estado: any;
  picture:any;
  stateDetailPage: any = StatePage;

  constructor(public navCtrl: NavController, public navParams: NavParams, public userservice: User, public storage: Storage) {
    this.id = this.navParams.data;


    let myid = localStorage.getItem("id");
    
      //console.log('Your id is', val);

      this.userservice.getById(myid).subscribe(data => { data = data.json(); //console.log(data[0]);

        this.name = data[0]["name"];
        this.lastname = data[0]["lastname"];
        this.id= data[0]["id"];
        this.estado = data[0]["estado"];
        this.cellphone = data[0]["cellphone"];
        this.picture=data[0]["picture"];

   
        
        },
        err => console.log(err)
      );
   
    
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad DetailProfilePage');
    console.log(this.id);


  }

}
