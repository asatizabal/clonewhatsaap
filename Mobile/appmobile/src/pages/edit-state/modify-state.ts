import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

import { User } from "../../providers/providers";
import { Storage } from '@ionic/storage';
import { MainPage } from "../pages";
import { Usuario } from "../../models/user";
/**
 * Generated class for the ModifyStatePage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@Component({
  selector: 'page-modify-state',
  templateUrl: 'modify-state.html',
})
export class ModifyStatePage {

  estado: string;
  toggled: boolean = false;
  emojitext: string;
  user: any;

  constructor(public navCtrl: NavController, public navParams: NavParams, public storage:Storage, public userService:User) {
    this.estado = this.navParams.data;
    console.log("estado:"+this.estado);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ModifyStatePage');
  }

  changeState(event) {
    this.estado = this.estado + " " + event.char;
  }

  saveChanges(){

    let myid = localStorage.getItem("id");
    
        this.user = new Usuario(myid,null,null,null,null,this.estado,null,null,null,null);


      this.userService.putStatus(this.user).subscribe(

        data => { 
          
          let respuesta = data.json();
          console.log(respuesta);
         
          if (respuesta) {
            this.navCtrl.setRoot(MainPage);
            

          } else{


          }       
        }
      );
   
    

  }
}
