import { Component, ViewChild } from '@angular/core';
import { NavController, NavParams, Content, Events, AlertController } from 'ionic-angular';
import { TabsPage } from "../tabs/tabs";
import { ConversationProvider } from "../../providers/conversation/conversation";
import { ChatMessage } from "../../models/conversation";
import { ContactProfilePage } from "../detail-contact-profile/detail-contact-profile";
import { Restriccion } from "../../models/restriccion";
import { RestriccionProvider } from "../../providers/restriction/restriction";

/**
 * Generated class for the ConversationPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */


@Component({
    selector: 'page-conversation',
    templateUrl: 'conversation.html',
})
export class ConversationPage {
    @ViewChild(Content) content: Content;
    messages: Array<any>;
    msgList: ChatMessage[] = [];
    userId: string;
    userName: string;
    userImgUrl: string;
    toUserId: string;
    toUserName: string;
    editorMsg: string = '';
    other: any;
    idchat: any;
    setinterval: boolean;
    interval: any;
    private restriccion: any;
    private idRestriccion: any;
    constructor(public navCtrl: NavController, public navParams: NavParams, public chatService: ConversationProvider, public events: Events, public restriccionProvider: RestriccionProvider, private alertCtrl: AlertController) {
        console.log(navParams.data);
        this.other = navParams.data;
        this.idchat = this.other["id"];
        console.log(this.other["id"]);
        this.toUserId = this.other["other"];
        this.userId = this.other["me"];
        this.messages = [];
        this.setinterval = true;
        this.toUserName = this.other["nombre"];
        
    }

    ionViewDidLoad() {
             if (this.setinterval) {
            this.interval = setInterval(() => {

                this.getMsg()
                    .then(() => {

                    });
            },
                1000);
        }


    }


    ionViewDidEnter() {

        this.getMsg()
            .then(() => {

            });

    }




    getMsg() {
        // Get mock message list
        return this.chatService
            .getMsgList(this.userId, this.toUserId)
            .then(res => {
                if (res) {
                    if (res.length > this.msgList.length) {
                        this.msgList = res;
                        console.log("es mayor");
                        this.scrollToBottom();
                    }
                }

            })
            .catch(err => {
                console.log(err)
            })
    }

    send(msj) {
        this.restriccion = new Restriccion(null, this.userId, this.toUserId, this.idRestriccion);
        this.restriccionProvider.postIdRestriccion(this.restriccion).subscribe(data => {
            let idArr = data.json();
            this.idRestriccion = idArr[0].id;

            if (this.idRestriccion == 2) {
                this.presentAlert();
            } else {

                let newMsg = {
                    Chat_id: this.other["id"],
                    estado_mensaje_id: 1,
                    fecha: Date.now(),
                    toUserId: this.toUserId,
                    user_id: this.userId,
                    mensaje: msj,
                    tipo_mensaje_id: 1
                };
                this.chatService.sendMsg(newMsg).subscribe(res => {
                });
            }
        });

    }

    presentAlert() {
        let alert = this.alertCtrl.create({
            title: 'Desbloquea a ' + this.toUserName + 'para enviar el mensaje!',
            buttons: ['Ok']
        });
        alert.present();
    }

    openAnotherProfile() {
        this.navCtrl.push(ContactProfilePage, {
            contact: this.other
        });
    }

    cancel() {
        this.setinterval = false;
        clearInterval(this.interval);
        setTimeout(() => {
            this.navCtrl.setRoot(TabsPage);
        }, 400);
    }


    scrollToBottom() {
        setTimeout(() => {
            if (this.content.scrollToBottom) {
                this.content.scrollToBottom();
            }
        }, 400)
    }

}
