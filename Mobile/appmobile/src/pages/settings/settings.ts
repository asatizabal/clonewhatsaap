import { Component, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { NavController, NavParams, Nav } from 'ionic-angular';
import { User } from "../../providers/providers";
import { Settings } from '../../providers/settings';
import { DetailProfilePage } from "../detail-profile/detail-profile";
import { TranslateService } from '@ngx-translate/core';
import { WelcomePage } from "../welcome/welcome";
import { BarcodeScanner } from '@ionic-native/barcode-scanner';

/**
 * The Settings page is a simple form that syncs with a Settings provider
 * to enable the user to customize settings for the app.
 *
 */
@Component({
  selector: 'page-settings',
  templateUrl: 'settings.html'
})
export class SettingsPage {
  @ViewChild(Nav) nav: Nav;
  picture: any;
  username: any;
  name:any;
  lastname: any;
  id: any;
  user: any;
  estado: any;
  detailProfile: any = DetailProfilePage;
  codedata:any;

  constructor(public navCtrl: NavController, public navParams: NavParams, public userservice:User,
    private barcodeScanner: BarcodeScanner ) {

    let myid = localStorage.getItem("id");
      //console.log('Your id is', val);

      this.userservice.getById(myid).subscribe(data => { data = data.json(); 
        console.log( data[0]["estado"]);

        this.name = data[0]["name"];
        this.lastname = data[0]["lastname"];
        this.id= data[0]["id"];
        this.picture= data[0]["picture"];
        this.estado = data[0]["estado"];
        
        },
        err => console.log(err)
      );
  


  }

  cerrar(){
    this.userservice.logout();
    window.location.reload();
     
    
    
    }
  
    web(){

 
  
      this.barcodeScanner.scan().then((barcodeData) => {
        
        let data = {barcodeData:barcodeData.text,id:this.id};
       this.userservice.insertarCodigo(data).subscribe(res=>{

        
       });
       
       }, (err) => {
           // An error occurred
       });
    }


  ionViewDidLoad() {
    console.log('ionViewDidLoad ProfilePage');

  }


}
