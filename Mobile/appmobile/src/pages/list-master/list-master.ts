import { Component } from '@angular/core';
import { NavController, ModalController, NavParams, AlertController } from 'ionic-angular';
import { ChatProvider } from "../../providers/chat/chat";
import { ItemCreatePage } from '../item-create/item-create';
import { ItemDetailPage } from '../item-detail/item-detail';
import { ContactsPage } from '../contacts/contacts';
import { Items } from '../../providers/providers';
import { Restriccion } from "../../models/restriccion";
import { Item } from '../../models/item';
import { ConversationPage } from "../conversation/conversation";
import { EditarChatsPage } from "../editar-chats/editar-chats";
import { RestriccionProvider } from "../../providers/restriction/restriction";
import { ContactsProvider } from "../../providers/contacts/contacts";
import { ConversationProvider } from "../../providers/conversation/conversation";

@Component({
  selector: 'page-list-master',
  templateUrl: 'list-master.html'
})
export class ListMasterPage {

  private currentItems2: Item[];
  private isABlockedUser: any = "false";
  private idRestriccion: any;
  private existe: any = "false";
  private currentItems: Item[];
  private id:any;
  private restriccion: Restriccion;
  interval: any;
  constructor(public navCtrl: NavController,private navParams: NavParams, public items: Items, public modalCtrl: ModalController,public misChats:ChatProvider,private alertCtrl: AlertController,public restriccionProvider: RestriccionProvider,public misContactos:ContactsProvider,public serviceCon:ConversationProvider) {
   
    this.id = localStorage.getItem("id");
    
    this.listarChats();
  }

  ionViewDidLoad() {
    this.listarChats();
    /*this.interval = setInterval(() => { 
      
      
                        },
                      3000);*/
  }


  newmessages(){

    
    let array =[];
    this.currentItems2.forEach(element => {
      let datos ={me:this.id,other:element["other"]}
     this.serviceCon.getnewChats(datos).subscribe(res=>{
      array.push(res[res.length-1]);
      //this.currentItems = res;
      console.log(array);
     //console.log(this.currentItems[2]["mensaje"]["mensaje"]);
      
     });
     //
  
    });



  }

  listarChats(){

    this.misChats.ListarChats(this.id).subscribe(res => {
 
  
      this.currentItems2 = res;
      console.log(this.currentItems2);
      this.newmessages();
    });

  }

  addItem() {
    let addModal = this.modalCtrl.create(ContactsPage);

 
    addModal.present();
  }

  deleteItem(item) {
    this.items.delete(item);
  }

  editar(){
    let addModal = this.modalCtrl.create(EditarChatsPage);
    clearInterval(this.interval);     
        addModal.present();
     
  }

  openItem(item: Item) {


console.log(item["token"]);
if(item["token"]==false){
  console.log("entro al if");
  
  
let alert = this.alertCtrl.create({
    title: '¿Deseas agregar este contacto a tu lista de contactos ?',
    buttons: [
        {
        text: 'Si',
        handler: () => {
          this.misContactos.guardarContacto( this.id, item["other"]).subscribe(res => {

            console.log(res);

            this.id = localStorage.getItem("id");
            this.restriccion = new Restriccion(null, this.id, item["other"], 1);
            this.restriccionProvider.putBlockUser(this.restriccion).subscribe(data => {
        
                 
              console.log(data)
            });

          });
          clearInterval(this.interval);
          let addModal = this.modalCtrl.create(ConversationPage,item);
          
          
                       addModal.present();
        }
    },
    {
        text: 'No',
        role: 'cancel',
      
        handler: () => {
         this.id = localStorage.getItem("id");
        this.restriccion = new Restriccion(null, this.id, item["other"], 1);
        console.log(this.restriccion);
        this.restriccionProvider.putBlockUser(this.restriccion).subscribe(data => {
        console.log(data)
                });

        console.log(item);
        clearInterval(this.interval);
        let addModal = this.modalCtrl.create(ConversationPage,item);


         addModal.present();
      }
    }
    ]

    });
    alert.present();
}else{


    console.log(item);
    let addModal = this.modalCtrl.create(ConversationPage,item);
    clearInterval(this.interval);

    addModal.present();

  }


  

  }
  
}
