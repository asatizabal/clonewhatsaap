import { Component } from '@angular/core';
import { NavController, ModalController } from 'ionic-angular';
import { Item } from "../../models/item";
import { ChatProvider } from "../../providers/chat/chat";
import { Items } from "../../providers/providers";

import { TabsPage } from "../tabs/tabs";
import { ListMasterPage } from "../list-master/list-master";

/**
 * Generated class for the EditarChatsPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@Component({
  selector: 'page-editar-chats',
  templateUrl: 'editar-chats.html',
})
export class EditarChatsPage {
  currentItems: Item[];
  private id:any;
  private estadoCheck:any=[];

  constructor(public navCtrl: NavController, public items: Items, public modalCtrl: ModalController,public misChats:ChatProvider) {
   
    this.id = localStorage.getItem("id");
    console.log("el bendito id"+this.id);
    this.ListarChats();
  }

  ionViewDidLoad() {
  }

  ListarChats(){

this.misChats.ListarChats(this.id).subscribe(res=>{
  console.log(res);

  this.currentItems=res;

});


  }



  delete() {
    console.log(this.estadoCheck);

    this.misChats.eliminarChats(this.estadoCheck).subscribe(
      
      
            data => {
              
              let respuesta = data.json();
              console.log(respuesta[0].id);
             
              if (respuesta[0].token == true) {
               
                alert("ERROR: No se ha podido eliminar el chat");
               } else {
                this.navCtrl.setRoot(ListMasterPage);
                
              }
      
            },
            error => console.log(error),
      
          )
  }

  cancel(){

    this.navCtrl.setRoot(TabsPage);
  }

  datachanged(e:any,item){
    console.log(e.checked);
    console.log(item);
    if(e.checked){
    this.estadoCheck.push(item);
    }else{
      let position = this.estadoCheck.indexOf(item);
      this.estadoCheck.splice(position, 1);
      
    }
}

  openItem(item: Item) {
   
  }
}
