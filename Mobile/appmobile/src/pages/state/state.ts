import { Component } from '@angular/core';
import {  NavController, NavParams } from 'ionic-angular';
import { ModifyStatePage } from "../edit-state/modify-state";

/**
 * Generated class for the StateDetailPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */


@Component({
  selector: 'page-state',
  templateUrl: 'state.html',
})
export class StatePage {

  estado: any;
  modifyStatePage: any = ModifyStatePage;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.estado = this.navParams.data;
    console.log(this.estado);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad StateDetailPage');
  }

}
