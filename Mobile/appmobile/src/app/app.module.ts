import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpModule, Http } from '@angular/http';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { Storage, IonicStorageModule } from '@ionic/storage';
import { ChatProvider } from "../providers/chat/chat";
import { MyApp } from './app.component';
import { EmojiPickerModule } from '@ionic-tools/emoji-picker';
import { ItemCreatePage } from '../pages/item-create/item-create';
import { ItemDetailPage } from '../pages/item-detail/item-detail';
import { ListMasterPage } from '../pages/list-master/list-master';
import { LoginPage } from '../pages/login/login';
import { SearchPage } from '../pages/search/search';
import { SettingsPage } from '../pages/settings/settings';
import { SignupPage } from '../pages/signup/signup';
import { TabsPage } from '../pages/tabs/tabs';
import { WelcomePage } from '../pages/welcome/welcome';
import {ChatboxModule} from "../chatbox";
import { Api } from '../providers/api';
import { Items } from '../mocks/providers/items';
import { Settings } from '../providers/settings';
import { User } from '../providers/user';

import { Camera } from '@ionic-native/camera';
import { GoogleMaps } from '@ionic-native/google-maps';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';

import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { ContactsProvider } from "../providers/contacts/contacts";
import { ContactsPage } from "../pages/contacts/contacts";
import { DetailProfilePage } from "../pages/detail-profile/detail-profile";
import { StatePage } from "../pages/state/state";
import { ModifyStatePage } from "../pages/edit-state/modify-state";
import { ConversationPage } from "../pages/conversation/conversation";
import { ConversationProvider } from '../providers/conversation/conversation';
import { ContactProfilePage } from "../pages/detail-contact-profile/detail-contact-profile";
import { TipoRestriccionProvider } from "../providers/typeRestriction/typeRestriction";
import { RestriccionProvider } from "../providers/restriction/restriction";
import { EditarChatsPage } from "../pages/editar-chats/editar-chats";
import { BarcodeScanner } from '@ionic-native/barcode-scanner';



// The translate loader needs to know where to load i18n files
// in Ionic's static asset pipeline.
export function HttpLoaderFactory(http: Http) {
  return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}

export function provideSettings(storage: Storage) {
  /**
   * The Settings provider takes a set of default settings for your app.
   *
   * You can add new settings options at any time. Once the settings are saved,
   * these values will not overwrite the saved values (this can be done manually if desired).
   */
  return new Settings(storage, {
    option1: true,
    option2: 'Ionitron J. Framework',
    option3: '3',
    option4: 'Hello'
  });
}

@NgModule({
  declarations: [
    MyApp,
    ItemCreatePage,
    ItemDetailPage,
    ListMasterPage,
    LoginPage,
    SearchPage,
    ContactsPage,
    SettingsPage,
    SignupPage,
    TabsPage,
    WelcomePage,
    DetailProfilePage,
    StatePage,
    ModifyStatePage,
    ConversationPage,
    ContactProfilePage,
    EditarChatsPage

  ],
  imports: [
    BrowserModule,
    HttpModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [Http]
      }
    }),
    IonicModule.forRoot(MyApp),
    EmojiPickerModule.forRoot(),
    ChatboxModule,
    IonicStorageModule.forRoot()
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    ItemCreatePage,
    ItemDetailPage,
    ListMasterPage,
    LoginPage,
    SearchPage,
    SettingsPage,
    SignupPage,
    TabsPage,
    WelcomePage,
    ContactsPage,
    DetailProfilePage,
    StatePage,
    ModifyStatePage,
    ConversationPage,
    ContactProfilePage,
    EditarChatsPage
  ],
  providers: [
    Api,
    Items,
    User,
    ChatProvider,
    ContactsProvider,
    TipoRestriccionProvider,
    RestriccionProvider,
    Camera,
    GoogleMaps,
    SplashScreen,
    StatusBar,
    BarcodeScanner,
    { provide: Settings, useFactory: provideSettings, deps: [Storage] },
    // Keep this to enable Ionic's runtime error handling during development
    { provide: ErrorHandler, useClass: IonicErrorHandler },
    ConversationProvider
  ]
})
export class AppModule { }
