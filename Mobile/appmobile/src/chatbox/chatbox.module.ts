import {NgModule} from '@angular/core';
import {IonicModule} from "ionic-angular";

import {ChatboxComponent} from "./chatbox.component";

@NgModule({
    imports: [
      
        IonicModule.forRoot(ChatboxComponent)
    ],
    exports: [ChatboxComponent],
    declarations: [ChatboxComponent],
    entryComponents: [ChatboxComponent],
    providers: [],
})
export class ChatboxModule { }
