import { Component, OnInit, ViewChild } from "@angular/core";
import { ConversationPage } from "../pages/conversation/conversation";
import { ConversationProvider } from "../providers/conversation/conversation";
import { ChatMessage } from "../models/conversation";
import { TextInput } from "ionic-angular";
import { Camera, CameraOptions } from '@ionic-native/camera';
import { Media, MediaObject } from '@ionic-native/media';

@Component({
    selector: 'chatbox',
    templateUrl: 'chatbox.component.html'
})

export class ChatboxComponent implements OnInit {
    msj :any;
    @ViewChild('chat_input') messageInput: TextInput;
    constructor(public msjes:ConversationPage,private camera: Camera) {
       
    }

    ngOnInit() {
    }


    send(){
          
                if(this.msj!=""){
                    this.msjes.send(this.msj);
                    this.msj=""
                    this.messageInput.setFocus();
                } 
          

       
    

    }

    sendbtn(){
            if(this.msj!=""){
         this.msjes.send(this.msj);
            this.msj=""
            this.messageInput.setFocus();
        }
    }

    startRecording($e) {
        console.log("agrabar");
        console.log($e);
        let media = new MediaObject('../Library/NoCloud/recording.wav');
        media.startRecord();
      }

    foto(){
        console.log("entrooo a la camara");
        const options: CameraOptions = {
            quality: 100,
            destinationType: this.camera.DestinationType.DATA_URL,
            encodingType: this.camera.EncodingType.JPEG,
            mediaType: this.camera.MediaType.PICTURE
          }
          
          this.camera.getPicture(options).then((imageData) => {
           // imageData is either a base64 encoded string or a file URI
           // If it's base64:
           let base64Image = 'data:image/jpeg;base64,' + imageData;
          }, (err) => {
           // Handle error
          });
    }
}