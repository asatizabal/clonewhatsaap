import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { ChatService } from "services/chatSevice";
import { ChatMessage } from "models/conversation";

@Component({
  selector: 'app-chat-detail',
  templateUrl: './chat-detail.component.html',
  styleUrls: ['./chat-detail.component.css']
})
export class ChatDetailComponent implements OnInit {

  //@ViewChild(Content) content: Content;
  messages: Array<any>;
  private msgList: ChatMessage[] = [];

  @Input() public viewing : any;
  private idChat: any;
  private setinterval: boolean;
  private interval: any;
  private userId: any;
  private toUserId: any;
  private fotoUser: any;
  private toUserName: any;
  public prueba : any;

  constructor(private chatService: ChatService) {
       
  

  console.log(this);

   /* if(this.viewing!=null){

    console.log("entra");
 this.idChat = this.viewing["id"];
    this.toUserId = this.viewing["other"];
    this.userId = this.viewing["me"];
    this.fotoUser = this.viewing["foto"];
    this.messages = [];
    this.setinterval = true;
    this.toUserName = this.viewing["nombre"];

    console.log(this.viewing);

      this.setinterval = true;
            if (this.setinterval) {
            this.interval = setInterval(() => {

                this.getMsg()
                    .then(() => {
                      
                    });
            },
                1000);
            }

    }*/

   }

  ngOnInit() {
    

  }

  getMsg() {
    this.userId = localStorage.getItem('user-id');

        return this.chatService
            .getMsgList(this.userId, this.toUserId)
            .then(res => {
              console.log(res);
                if(res){
                    if(res.length>this.msgList.length){
                        //this.scrollToBottom();
                       console.log(res);
                        this.msgList = res;

                    }
                }
         
            })
            .catch(err => {
                console.log(err);
            })
    }
  
  /*scrollToBottom() {
        setTimeout(() => {
            if (this.content.scrollToBottom) {
                this.content.scrollToBottom();
            }
        }, 400);
  }*/


  getNotification(data) {
    console.log("getting a notification: "+data);
  }

  getChange(data){
    console.log("Llega esto desde mi hermano aside: "+data);
  }

  
}
