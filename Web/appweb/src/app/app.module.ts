import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import { ChatsComponent } from './chats/chats.component';
import { ChatDetailComponent } from './chat-detail/chat-detail.component';
import { ChatService } from "services/chatSevice";
import { InitComponent } from './init/init.component';
import { NgxQRCodeModule } from 'ngx-qrcode2';
import { UserService } from "services/userSevice";
import { RouterModule, Routes } from '@angular/router';

const rutas: Routes = [
  { path: 'chat',      component: InitComponent }
];

@NgModule({
  declarations: [
    AppComponent,
    ChatsComponent,
    ChatDetailComponent,
    InitComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    NgxQRCodeModule,
    RouterModule.forRoot(rutas),
  ],
  providers: [ChatService,UserService],
  bootstrap: [AppComponent]
})
export class AppModule {




 }
