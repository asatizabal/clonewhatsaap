import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { ChatService } from "services/chatSevice";
import { Item } from "models/item";
import { ChatDetailComponent } from "app/chat-detail/chat-detail.component";


@Component({
  selector: 'app-chats',
  templateUrl: './chats.component.html',
  styleUrls: ['./chats.component.css']
})
export class ChatsComponent implements OnInit {

  @Input()
  public selected: String;
  
  @Output()
  public changeContent: EventEmitter<any> = new EventEmitter();

  private idUser: any = 4;
  private chats: any;

  constructor(private chatService: ChatService) {
      chatService.getListarChats(this.idUser).subscribe(res => {
      this.chats = res.json();
      console.log(this.chats);
    });
   }

  ngOnInit() {

    localStorage.setItem('user-id', this.idUser);
  }

  loadComponent(component){
    this.changeContent.emit(component);

    
  }

}
