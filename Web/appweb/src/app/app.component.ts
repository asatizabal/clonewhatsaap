import { Component, Input } from '@angular/core';
import { UserService } from "services/userSevice";
import 'rxjs';
import {RouterModule,Router} from '@angular/router';



@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  elementType : 'url' | 'canvas' | 'img' = 'url';
  value : string ;

  constructor(private userSevice:UserService,private ruta:Router){
    
    this.value= Date.now().toString(); 
    console.log(this.value);
    this.search();
  }


    search(){

      let interval=setInterval(()=>{
        this.userSevice.getQrUser(this.value).subscribe(res=>{
         console.log(res);
         if(res.length>0){
          if(res[0]["usuario_id"]){
            console.log("entroo");
            localStorage.setItem("idUser-webwhatsaap",res[0]["usuario_id"]);
          clearInterval(interval);
            this.ruta.navigateByUrl("/chat");
          }  }
        });
      
    


      },1000);


    }





}
