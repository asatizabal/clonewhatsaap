import { Component, OnInit } from '@angular/core';
import { ChatService } from "services/chatSevice";
import { ChatMessage } from "models/conversation";

@Component({
  selector: 'app-init',
  templateUrl: './init.component.html',
  styleUrls: ['./init.component.css']
})
export class InitComponent {

  messages: Array<any>;
  private msgList: ChatMessage[] = [];

  private idUser: any ;
  private chats: any;
  private listarchat:boolean=false;
  private inicial:boolean=true;
  private idChat: any;
  private setinterval: boolean;
  private interval: any;
  private userId: any;
  private toUserId: any;
  private fotoUser: any;
  private toUserName: any;
  public prueba : any;
  
 constructor(private chatService: ChatService) {
    this.idUser= localStorage.getItem("idUser-webwhatsaap");
      chatService.getListarChats(this.idUser).subscribe(res => {
      this.chats = res.json();
      console.log(this.chats);
    });
   }


  
  getMsg() {
    

        return this.chatService
            .getMsgList(this.userId, this.toUserId)
            .then(res => {
              console.log(res);
                if(res){
                    if(res.length>this.msgList.length){
                        //this.scrollToBottom();
                       console.log(res);
                        this.msgList = res;

                    }
                }
         
            })
            .catch(err => {
                console.log(err);
            })
    }

  loadChat(chat){
    console.log(chat);

    this.listarchat=true;
    this.inicial=false;



 this.idChat = chat["id"];
    this.toUserId = chat["other"];
    this.userId = chat["me"];
    this.fotoUser = chat["foto"];
    this.messages = [];
    this.setinterval = true;
    this.toUserName = chat["nombre"];

    console.log(chat);
   this.getMsg().then(() => {
                      
                    });

      /*this.setinterval = true;
            if (this.setinterval) {
            this.interval = setInterval(() => {

                this.getMsg()
                    .then(() => {
                      
                    });
            },
                1000);
            }*/

    
  }
}
 