import { Injectable } from '@angular/core';
import { Http, RequestOptions, Headers } from '@angular/http';

import 'rxjs';

import { config } from "./config";
import { ChatMessage } from "models/conversation";


@Injectable()
export class UserService {

  constructor( private http: Http) { }

  getQrUser(value) {
    let url = config.SERVICES+"Qr/Qr?value=" +value;
    console.log(url);
    return this.http.get(url).map(res=> res.json());
  }


  getMsgList(id,id2): Promise<ChatMessage[]> {
    let url=config.SERVICES+"Mensaje/Mensajes?id="+id+"&id2="+id2;
    console.log(url);
      return this.http.get(url).toPromise()
        .then(response => response.json() as ChatMessage[])
        .catch(err => Promise.reject(err || 'err'));

}

  postSendMsg(msg) {
    let url = config.SERVICES+"Mensaje/GuardarMensaje";
    let headers=new Headers({'Content-Type':'application/x-www-form-urlencoded'});
  
    let data=this.jsonToUrlParams(msg);
    console.log("llego al servico de mandar" + data);
    let options=new RequestOptions({headers:headers});
    return this.http.post(url, data,{headers:headers}).map(res=>{
      console.log("hola");
    });
    
  }

jsonToUrlParams(json){
  
        var str=Object.keys(json)
                .map(key => encodeURIComponent(key) + '=' + encodeURIComponent(json[key]))
                .join("&");
  
        return ( str);
  
    }

}
