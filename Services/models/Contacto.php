
<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Model
 *
 * @author Pabhoz
 */
class Contacto extends Model {

    protected static $table = "Contacto";

  private $me;
  private $other;


  


      function __construct($me, $other) {
          $this->me = $me;
          $this->other = $other;
        
      }

      
      function getMe() {
          return $this->me;
      }

      function getOther() {
          return $this->other;
      }

  
      function setMe($me) {
          $this->me = $me;
      }

      function setOther($other) {
          $this->other = $other;
      }

      
            
  public function getMyVars(){
        return get_object_vars($this);
    }

}
