<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Model
 *
 * @author Pabhoz
 */
class Mensaje extends Model {
    
    protected static $table = "Mensaje";

  private $id;
  private $mensaje;
  private $fecha;
  private $tipo_mensaje_id;
  private $estado_mensaje_id;
  private $Chat_id;

  
  
  private $has_one = array(
      'Tipo_mensaje'=>array(
          'class'=>'Tipo_mensaje',
          'join_as'=>'$tipo_mensaje_id',
          'join_with'=>'id'
          ),
       'Estado_mensaje'=>array(
          'class'=>'Estado_mensaje',
          'join_as'=>'$estado_mensaje_id',
          'join_with'=>'id'
          )
      );
  

      function getHas_one() {
          return $this->has_one;
      }

      function setHas_one($has_one) {
          $this->has_one = $has_one;
      }

       
  
      function __construct($id, $mensaje, $fecha, $tipo_mensaje_id, $estado_mensaje_id, $Chat_id) {
          $this->id = $id;
          $this->mensaje = $mensaje;
          $this->fecha = $fecha;
          $this->tipo_mensaje_id = $tipo_mensaje_id;
          $this->estado_mensaje_id = $estado_mensaje_id;
          $this->Chat_id = $Chat_id;
         
      }

      function getId() {
          return $this->id;
      }

      function getMensaje() {
          return $this->mensaje;
      }

      function getFecha() {
          return $this->fecha;
      }

      function getTipo_mensaje_id() {
          return $this->tipo_mensaje_id;
      }

      function getEstado_mensaje_id() {
          return $this->estado_mensaje_id;
      }

     



      function setId($id) {
          $this->id = $id;
      }

      function setMensaje($mensaje) {
          $this->mensaje = $mensaje;
      }

      function setFecha($fecha) {
          $this->fecha = $fecha;
      }

      function setTipo_mensaje_id($tipo_mensaje_id) {
          $this->tipo_mensaje_id = $tipo_mensaje_id;
      }

      function setEstado_mensaje_id($estado_mensaje_id) {
          $this->estado_mensaje_id = $estado_mensaje_id;
      }

      function getChat_id() {
          return $this->Chat_id;
      }

      function setChat_id($Chat_id) {
          $this->Chat_id = $Chat_id;
      }

      
   
      

            
      public function getMyVars(){
        return get_object_vars($this);
    }

}

