<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Model
 *
 * @author Pabhoz
 */
class Notificacion extends BModel {

  private $id;
  private $event;
  private $estado_notificacion_id;
  private $usario_id;
  
  

  private $has_one = array(
      'RuleName'=>array(
          'class'=>'[Obj class expected]',
          'join_as'=>'[foreign key attr]',
          'join_with'=>'[obj primary key attr]'
          )
      );
  
  private $known_as = array(
        
            'Owner' => array(
                'class' => '[Obj class expected]',
                'join_as' => '[my key attr]',
                'join_with' => '[foreign key attr]'
            )
        
        );

  private $has_many = array(
      'RuleName'=>array(
          'class'=>'[Obj class expected]',
          'my_key'=>'[my primary key attr]',
          'other_key'=>'[the other entity primary key attr]',
          'join_as'=>'[my attr at n to n table]',
          'join_with'=>'[the other attr at n to n table]',
          'join_table'=>'[N to N table name]',
          'data'=> array(
             '[table attr]'=>'[variable type demo]' // 'aFloat' => 0.0, 'aString' => '' 
            )
          )
      );


            
      function __construct($id, $event, $estado_notificacion_id,$usuario_id) {
          $this->id = $id;
          $this->event = $event;
          $this->estado_notificacion_id = $estado_notificacion_id;
          $this->usuario_id = $usuario_id;
   
      }
      function getId() {
          return $this->id;
      }

      function getEvent() {
          return $this->event;
      }

      function getEstado_notificacion_id() {
          return $this->estado_notificacion_id;
      }

      function getUsario_id() {
          return $this->usario_id;
      }

      function setUsario_id($usario_id) {
          $this->usario_id = $usario_id;
      }

      
      function getHas_one() {
          return $this->has_one;
      }

      function getKnown_as() {
          return $this->known_as;
      }

      function getHas_many() {
          return $this->has_many;
      }

      function setId($id) {
          $this->id = $id;
      }

      function setEvent($event) {
          $this->event = $event;
      }

      function setEstado_notificacion_id($estado_notificacion_id) {
          $this->estado_notificacion_id = $estado_notificacion_id;
      }

    

      function setHas_one($has_one) {
          $this->has_one = $has_one;
      }

      function setKnown_as($known_as) {
          $this->known_as = $known_as;
      }

      function setHas_many($has_many) {
          $this->has_many = $has_many;
      }

      
      public function getMyVars(){
        return get_object_vars($this);
    }

}

