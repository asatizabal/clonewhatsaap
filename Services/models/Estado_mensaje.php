<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Model
 *
 * @author Pabhoz
 */
class Estado_mensaje extends BModel {

  private $id;
  private $descripcion;


  
      function __construct($id, $descripcion) {
          $this->id = $id;
          $this->descripcion = $descripcion;
     
      }

      
      function getId() {
          return $this->id;
      }

      function getDescripcion() {
          return $this->descripcion;
      }

      function getHas_one() {
          return $this->has_one;
      }

     

      function setId($id) {
          $this->id = $id;
      }

      function setDescripcion($descripcion) {
          $this->descripcion = $descripcion;
      }

      function setHas_one($has_one) {
          $this->has_one = $has_one;
      }

    

            
    public function getMyVars(){
        return get_object_vars($this);
    }

}
