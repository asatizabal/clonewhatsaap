<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Model
 *
 * @author Pabhoz
 */
class Usuario extends BModel {

  private $id;
  private $username;
  private $password;
  private $cellphone;
  private $picture;
  private $estado;
  private $estado_usuario_id;
  private $name;
  private $lastname;
  private $email;
  
  

      function __construct($id, $username, $password, $cellphone, $picture, $estado, $estado_usuario_id, $name, $lastname, $email) {
          $this->id = $id;
          $this->username = $username;
          $this->password = $password;
          $this->cellphone = $cellphone;
          $this->picture = $picture;
          $this->estado = $estado;
          $this->estado_usuario_id = $estado_usuario_id;
          $this->name = $name;
          $this->lastname = $lastname;
          $this->email = $email;
       
      }
      
      function getId() {
          return $this->id;
      }

      function getUsername() {
          return $this->username;
      }

      function getPassword() {
          return $this->password;
      }

      function getCellphone() {
          return $this->cellphone;
      }

      function getPicture() {
          return $this->picture;
      }

      function getEstado() {
          return $this->estado;
      }

      function getEstado_usuario_id() {
          return $this->estado_usuario_id;
      }

      function getName() {
          return $this->name;
      }

      function getLastname() {
          return $this->lastname;
      }

      function getEmail() {
          return $this->email;
      }

     
      function setId($id) {
          $this->id = $id;
      }

      function setUsername($username) {
          $this->username = $username;
      }

      function setPassword($password) {
          $this->password = $password;
      }

      function setCellphone($cellphone) {
          $this->cellphone = $cellphone;
      }

      function setPicture($picture) {
          $this->picture = $picture;
      }

      function setEstado($estado) {
          $this->estado = $estado;
      }

      function setEstado_usuario_id($estado_usuario_id) {
          $this->estado_usuario_id = $estado_usuario_id;
      }

      function setName($name) {
          $this->name = $name;
      }

      function setLastname($lastname) {
          $this->lastname = $lastname;
      }

      function setEmail($email) {
          $this->email = $email;
      }

     
      
          public function getMyVars(){
        return get_object_vars($this);
    }

}
