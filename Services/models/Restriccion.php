<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Model
 *
 * @author Pabhoz
 */
class Restriccion extends Model {
  
  protected static $table = "Restriccion";
  private $id;
  private $owner;
  private $destinatario;
  private $tipo_restriccion_id;
  
    private $has_one = array(
      'Tipo_restriccion'=>array(
          'class'=>'Tipo_restriccion',
          'join_as'=>'$tipo_restriccion_id',
          'join_with'=>'id'
          )
      );
  

 
      function __construct($id, $owner, $destinatario, $tipo_restriccion_id) {
          $this->id = $id;
          $this->owner = $owner;
          $this->destinatario = $destinatario;
          $this->tipo_restriccion_id = $tipo_restriccion_id;
     
      }
 
      static function getTable() {
          return self::$table;
      }

      function getId() {
          return $this->id;
      }

      function getOwner() {
          return $this->owner;
      }

      function getDestinatario() {
          return $this->destinatario;
      }

      function getTipo_restriccion_id() {
          return $this->tipo_restriccion_id;
      }

      static function setTable($table) {
          self::$table = $table;
      }

      function setId($id) {
          $this->id = $id;
      }

      function setOwner($owner) {
          $this->owner = $owner;
      }

      function setDestinatario($destinatario) {
          $this->destinatario = $destinatario;
      }

      function setTipo_restriccion_id($tipo_restriccion_id) {
          $this->tipo_restriccion_id = $tipo_restriccion_id;
      }

                    
    public function getMyVars(){
        return get_object_vars($this);
    }

}