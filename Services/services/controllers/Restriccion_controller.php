<?php

class Restriccion_controller extends BServiceController {

    function __construct() {
        parent::__construct();
    }
    
    
    public function getIndex() {
        Request::setHeader(202, "text/html");
        echo "Get method Index controller";
       // $users = User_bl::getAll();
       // Penelope::printJSON($users);
        
    }
    
    public function postValidar(){
        
        Request::setHeader(202, "text/html");
        
        $restriccionArr = $_POST;
        
        $response = Restriccion_bl::buscarRestriccion($restriccionArr);
        
        //var_dump($restriccionArr);
        Penelope::printJSON($response);
        
    }
   
    public function putRestriccion(){
        
        Request::setHeader(202, "text/json");
        
        //echo "dnkjvdf";
        $_PUT = $this->_PUT;
        $id_restriccion = $_PUT["idDescripcion"];
        $owner = $_PUT["owner"];
        $destinatario = $_PUT["destinatario"];
        
        //var_dump($destinatario);
        
        $response = Restriccion_bl::crearRestriccion($id_restriccion,$owner,$destinatario);

        //var_dump($id_restriccion);
        Penelope::printJSON($response);
        
        
    }
    
     public function putDesbloquear(){
        
        Request::setHeader(202, "text/json");
        
        $_PUT = $this->_PUT;
        $id_restriccion = $_PUT["idDescripcion"];
        $owner = $_PUT["owner"];
        $destinatario = $_PUT["destinatario"];
        
        //var_dump($destinatario);
        
        $response = Restriccion_bl::desbloquearRestriccion($id_restriccion,$owner,$destinatario);

        //var_dump($id_restriccion);
        Penelope::printJSON($response);
        
        
    }
    
    
    
  
}
