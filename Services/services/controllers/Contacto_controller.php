<?php

class Contacto_controller extends BServiceController {

    function __construct() {
        parent::__construct();
    }
    
   
  

    public function getListarContactos($id) {
        Request::setHeader(202, "text/json");
        $id=$_GET["id"];
        $response = Contacto_bl::allContacts($id);
        Penelope::printJSON($response);
       
    }



    public function getVerificarContactos($id,$id2) {
        Request::setHeader(202, "text/json");
        $id=$_GET["id"];
        $id2=$_GET["id2"];

        $response = Contacto_bl::misContactos($id,$id2);
        Penelope::printJSON($response);
       
    }
    
    public function getGuardarContacto($id){
        
        Request::setHeader(202, "text/json");
         $id=$_GET["id"];
        $id2=$_GET["id2"];
        $response = Contacto_bl::crearContacto($id,$id2);
        Penelope::printJSON($response);
        
      
        
    }
    
    
    
}
