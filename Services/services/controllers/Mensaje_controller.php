<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
class Mensaje_controller extends BServiceController {

    function __construct() {
        parent::__construct();
    }
    
    

    public function getMensajes() {
        Request::setHeader(202, "text/json");
        $id=$_GET["id"];
        $id2=$_GET["id2"];
        
        $response = Mensaje_bl::allMessages($id,$id2);
        Penelope::printJSON($response);
       
    }
    
    public function postGuardarMensaje() {
            
        Request::setHeader(202, "text/json");
        $array = $_POST;
       
        $r= Mensaje_bl::agregarMensaje($array);
        Penelope::printJSON($r);
        
        
    }

    public function postNewMessages(){

        Request::setHeader(202, "text/json");
        $array = $_POST;
        $r= Mensaje_bl::newMessages($array);
        Penelope::printJSON($r);

    }
}