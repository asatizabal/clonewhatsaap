<?php

class Estado_usuario_controller extends BServiceController {

    function __construct() {
        parent::__construct();
    }
    
   
    public function getIndex() {
        Request::setHeader(202, "text/html");
        //echo "Get method Index controller";
        $estados = Estado_usuario_bl::getAll();
        Penelope::printJSON($estados);
        
    }
    
    
}
