<?php

class User_controller extends BServiceController {

    function __construct() {
        parent::__construct();
    }
    
     public function getById($id){
        Request::setHeader(202, "text/json");
        $r = User_bl::getById($id);
        Penelope::printJSON($r);   
    }
    
    public function putStatus(){
        Request::setHeader(202, "text/json");
        $_PUT = $this->_PUT;
        $estado = $_PUT["estado"];
        $id = $_PUT["id"];
        
        $response = User_bl::updateStatus($id,$estado);

        var_dump($response);
        //Penelope::printJSON($response);
        
    }

    public function postLogin() {
    
        Request::setHeader(202, "text/json");
        $userArr = $_POST;
       
        $r= User_bl::login($userArr);
        Penelope::printJSON($r);
    
         
         
    } 
    
}
