<?php

class Qr_controller extends BServiceController {

    function __construct() {
        parent::__construct();
    }
    
   
 

    public function getQr() {
        Request::setHeader(202, "text/json");
         $id=$_GET["value"];
            $response = Qr_bl::qr($id);
        Penelope::printJSON($response);
       
    }

     

    public function postCrearQr() {
        Request::setHeader(202, "text/json");
         $code=$_POST["barcodeData"];
         $id=$_POST["id"];
            $response = Qr_bl::insertar($code,$id);
        Penelope::printJSON($response);
       
    }

 
    
 
    
  
}
