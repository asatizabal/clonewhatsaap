<?php

class Chat_controller extends BServiceController {

    function __construct() {
        parent::__construct();
    }
    
   
 

    public function getListarChats($id) {
        Request::setHeader(202, "text/json");
        $id=$_GET["id"];
            $response = Chat_bl::allChats($id);
        Penelope::printJSON($response);
       
    }

 

    public function  postCrearChat(){
        Request::setHeader(202, "text/json");
        $chatsArr = $_POST;
        $r= Chat_bl::crear($chatsArr);
        Penelope::printJSON($r);

    }
    
       public function postEliminarChats() {
    
        Request::setHeader(202, "text/json");
        $chatsArr = $_POST;
       
       
        $r= Chat_bl::delete($chatsArr);
        Penelope::printJSON($r);
    
         
         
    } 
    
 
    
  
}
